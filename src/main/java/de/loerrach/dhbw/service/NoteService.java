package de.loerrach.dhbw.service;

import de.loerrach.dhbw.entities.Note;
import de.loerrach.dhbw.repos.NoteRepository;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

//Service-Klasse
public class NoteService {

    //Logger
    private final Logger LOGGER = Logger.getLogger(NoteService.class.getName());

    //Aufruf Repository
    @Inject
    private NoteRepository noteRepository;


    //Save
    public Boolean saveNote(Note note){
        try{
            noteRepository.persistNote(note);
            return true;
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    //Edit-Funktion
    public Boolean editNote(Note note){
        try{
            noteRepository.editNote(note);
            return true;
        } catch (Exception e){
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    //Delete-Funktion
    public boolean deleteNoteByID(Long id){
        try{
            noteRepository.deleteNoteByID(id);
            return true;
        } catch (Exception e){
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    //Get-Funktion
    public Note findByID(Long id){
        try{
            return noteRepository.findNoteByID(id);
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    //Get-All-Funktion
    public List<Note> getAllNotes(){
        try{
            return noteRepository.findAll();
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return null;
        }
    }
}
