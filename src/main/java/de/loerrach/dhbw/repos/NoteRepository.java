package de.loerrach.dhbw.repos;

import de.loerrach.dhbw.entities.Note;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

//Repository-Klasse für DB-Access
public class NoteRepository {

    //Aufruf Entity-Manager
    @PersistenceContext(unitName = "notes")
    private EntityManager entityManager;

    //DB-Funktionen

    //Post
    @Transactional
    public void persistNote(Note note) {
        entityManager.persist(note);
    }

    //Update
    @Transactional
    public void editNote(Note note) {
        entityManager.merge(note);
    }

    //Delete
    @Transactional
    public void deleteNoteByID(Long id) {

        Note noteDeletable = entityManager.find(Note.class, id);

        entityManager.remove(noteDeletable);
    }

    //Get
    public Note findNoteByID(Long id) {
        return entityManager.find(Note.class, id);
    }


    //DB-Funktion um alle Items zu bekommen (SQL)
    public List<Note> findAll() {
        Query query = entityManager.createQuery("SELECT n FROM Note n");
        return query.getResultList();
    }
}
