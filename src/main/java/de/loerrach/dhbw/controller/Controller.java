package de.loerrach.dhbw.controller;

import de.loerrach.dhbw.entities.Note;
import de.loerrach.dhbw.service.NoteService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("/api") //Basispfad
@Produces(MediaType.APPLICATION_JSON)
public class Controller {

    //Logger
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    //Implementierung NoteService
    @Inject
    private NoteService noteService;

    //Hello-World-Ausgabe zum Testen des Backend
    @GET
    @Path("/hello")
    public String getHelloWorld()
    {
        return "Hello World";
    }

    //Get-Methode um Item nach ID zu finden
    @GET
    @Path("/note")
    public Note getNote(@QueryParam("id") Long id)
    {
        return noteService.findByID(id);
    }

    //Get-Methode um alle ListItems zu bekommen
    @GET
    @Path("/note/all")
    public List<Note> getAllNotes()
    {
        return noteService.getAllNotes();
    }


    //Post-Methode um Item in DB zu speichern
    @POST
    @Path("/note")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postNote(@Valid Note note)
    {
        LOGGER.info("posted note " + note.getTitle() + " and text: " + note.getText() + " with ID:" + note.getId());

        Boolean success = noteService.saveNote(note);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        return Response.status(responseStatus).build();
    }


    //Delete-Methode um Items nach ID zu löschen
    @DELETE
    @Path("/note")
    public Response deleteNote(@QueryParam("id") Long id)
    {

        Boolean success = noteService.deleteNoteByID(id);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        return Response.status(responseStatus).build();
    }

    //Put-Methode um diese nach Query-Parametern zu ändern --> funktioniert noch nicht
    @PUT
    @Path("/note")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editNote(@QueryParam("id") Long id, @QueryParam("title") String title, @QueryParam("text") String text)
    {

        Note newNote = noteService.findByID(id);
        newNote.setTitle(title);
        newNote.setText(text);
        noteService.editNote(newNote);
        Boolean success = noteService.editNote(newNote);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        return Response.status(responseStatus).build();
    }


}
